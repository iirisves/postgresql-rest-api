import React, {Component} from 'react';
import {Navbar, Container} from 'react-bootstrap';
import './BottomMenu.css';

export class BottomMenu extends Component {
    render() {
        return (
            <Navbar fixed="bottom" className="bottomMenu" bg="primary" variant="dark">

            <Navbar.Text>
            Made by Iiris Vesala. <a href="https://gitlab.com/iirisves/postgresql-rest-api">GitLab repository</a>
            </Navbar.Text>

            </Navbar>
    )
    }
}

export default BottomMenu;