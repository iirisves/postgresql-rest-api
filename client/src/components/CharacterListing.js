import React, {Component} from 'react';
import {Table} from 'react-bootstrap';

export class CharacterListing extends Component {

    constructor(props) {
        super(props);
        this.state={users : []};
    }

   componentDidMount() {
        this.refreshList();
    }

    refreshList() {
        fetch('http://localhost:5000/api/db', {
            method: 'GET'
        })
            .then(response => response.json())
            .then((result) => {
                    this.setState({users : result['data']});
                }
            );
    }

    /*componentDidUpdate(prevProps, prevState) {
        //const {users, Id, First, Last, Age} = this.state;
        //Have to have comparison, otherwise you'd keep updating the page forever
        if (prevState.users !== this.state.users) {
            this.refreshList();
          }
        
    }*/

    render() {
        const { users } = this.state;

        return (
            <div>
            <h1>Common Disney characters</h1>
                <Table>
                    <thead>
                    <tr>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Year of first appearance</th>
                    </tr>
                    </thead>
                    <tbody>
                    {users.map(user=>
                        <tr key = {user.id}>
                            <td>{user.first}</td>
                            <td>{user.last}</td>
                            <td>{user.year}</td>
                        </tr>
                    )
                    }
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default CharacterListing;