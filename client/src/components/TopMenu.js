import React, {Component} from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import './TopMenu.css';

export class TopMenu extends Component {
    render() {
        return (
            <Navbar className="topMenuBar" bg="primary" variant="dark">
                <Nav className="mr-auto">
                    <Nav.Link href="/">Home</Nav.Link>
                    <Nav.Link href='/api/db'>List of Disney characters</Nav.Link>
                </Nav>
            </Navbar>
        )
    }
}


export default TopMenu;