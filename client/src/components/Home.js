import React, {Component} from 'react';

export class Home extends Component {
    constructor(props) {
        super(props);
    }

    render(){
        return (
            <div>
                <h1>Home</h1>
                <p>On this website, you will find a small database by clicking "List of Disney characters" in the menu above.</p>
                <p>This site was made using Node.js, React, Bootstrap and PostgreSQL.</p>
            </div>
        );
    }
}

export default Home;