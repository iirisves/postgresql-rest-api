import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './components/Home';
import TopMenu from './components/TopMenu';
import BottomMenu from './components/BottomMenu';
import CharacterListing from './components/CharacterListing';
import './App.css';

function App() {
    return (
        <BrowserRouter>
            <TopMenu/>
            <Switch>
                <Route path='/' component={Home} exact/>
                <Route path='/api/db' component={CharacterListing} />
            </Switch>
    <BottomMenu/>
        </BrowserRouter>
    );
}

export default App;
