const { Pool } = require('pg');
const pool = new Pool({
    user: 'me',
    host: 'localhost',
    database: 'api',
    password: 'password',
    port: 5432
    /* IMPORTANT:
In production, you should place these configuration settings ^ in a separate file that is not accessible to everyone!
 */
});

/*
const createDatabase = (req, res) => {
    pool.query('CREATE DATABASE api', (error, results) => {
        if (error) {
            throw error;
        }
        res.send('Database created.');
        console.log('Added database');
    })
}

const createTable = (req, res) => {
    pool.query('CREATE TABLE users (ID SERIAL PRIMARY KEY, first VARCHAR(255), last VARCHAR(255), year INT)', (error, results) => {
        if (error) {
            throw error;
        }
        res.send('Table added to database');
        console.log('Added table');
    })
}

const insertData = (req, res) => {
    pool.query('INSERT INTO users (first, last, year) VALUES (\'Donald\', \'Duck\', 1934), (\'Daisy\', \'Duck\', 1940), ('Scrooge', 'McDuck', 1947)',
        (error, results) => {
        if (error) {
            throw error;
        }
        res.send('Users added');
        console.log('Inserted data');
    })
}
*/

const getUsers = (req, res) => {
    pool.query('SELECT * FROM users ORDER BY id ASC', (error, results) => {
        if (error) {
            throw error
        }
        res.send({error: false, data: results.rows});
    })
}

const getUserById = (req, res) => {
    const id = parseInt(req.params.id);

    pool.query('SELECT * FROM users WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error;
        }
        res.status(200).json(results.rows);
    })
};

const createUser = (req, res) => {
    const { first, last, year } = req.body;

    pool.query('INSERT INTO users (first, last, year) VALUES ($1, $2, $3) RETURNING id', [first, last, year], (error, results) => {
        if (error) {
            throw error;
        }
        res.status(201).send('User added with id ' + results.rows[0].id); //not sure if this is the best way to get the id but it works
    })
};

const updateUser = (req, res) => {
    const id = parseInt(req.params.id);
    const { first, last, year } = req.body;

    pool.query('UPDATE users SET first = $1, last = $2, year = $3 WHERE id = $4', [first, last, year, id], (error, results) => {
        if (error) {
            throw error;
        }
        res.status(200).send('User modified with id ' + id);
    })
};

const deleteUser = (req, res) => {
    const id = parseInt(req.params.id);

    pool.query('DELETE FROM users WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error;
        }
        res.status(200).send('User deleted with id ' + id);
    })
};

module.exports = {
    //createDatabase,
    //createTable,
    //insertData,
    getUsers,
    getUserById,
    createUser,
    updateUser,
    deleteUser
};