const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const db = require('./queries');
const port = 5000;


app.use(bodyParser.json());
app.use(bodyParser.urlencoded(
    {extended: true}
    ));
app.use(cors());

app.get('/api/db', db.getUsers);
app.get('/api/users/:id', db.getUserById);
//app.get('/api/createdb', db.createDatabase);
//app.get('/api/createtable', db.createTable);
//app.get('/api/insertdata', db.insertData);
app.post('/api/users', db.createUser);
app.put('/api/users/:id', db.updateUser);
app.delete('/api/users/:id', db.deleteUser);

app.listen(port, () => {
    console.log('App running on port ' + port + '.')
});

// error handler
app.use(function(err, req, res) {
    res.status(err.status || 500)
        .json({
            status: 'error',
            message: err.message
        });
});

module.exports = app;

