Made by Iiris Vesala. 

Many thanks to Tania Rascia, whose tutorial in setting up a Node.js & PostgreSQL REST API was very helpful: https://blog.logrocket.com/setting-up-a-restful-api-with-node-js-and-postgresql-d96d6fc892d8/

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts
## To run locally
Make sure you have [Node.js](http://nodejs.org/) and [PostgreSQL] (https://www.enterprisedb.com/downloads/postgres-postgresql-downloads) installed.

You need to create a local PostgreSQL database. After installing Postgre, open it in the command prompt with
```
psql -U postgres
```
 or just 
 ```
 psql
```

Run 
```
CREATE ROLE me WITH LOGIN PASSWORD 'password';
ALTER ROLE me CREATEDB;
\q
psql -d postgres -U me
CREATE DATABASE api;
\c api
CREATE TABLE users (ID SERIAL PRIMARY KEY, first VARCHAR(255), last VARCHAR(255), year INT);
INSERT INTO users (first, last, year) VALUES ('Donald', 'Duck', 1934), ('Daisy', 'Duck', 1940);
\q
```

```
git clone https://gitlab.com/iirisves/postgresql-rest-api.git
cd postgresql-rest-api/src
npm install
node server.js
```
Then, open another command prompt and run:
```
cd postgresql-rest-api/src/client
npm install
npm start
```

If the browser doesn't open automatically, open [http://localhost:3000](http://localhost:3000) to view the app.
